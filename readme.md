# README #

This README describes which steps are necessary to get the demo application up and running.
First the Homestead vagrant dev enviroment is being build and also the corresponding configuration will be explained.

* Key Features Laravel
* Database Migrations with Artisan
* ORM Eloquent
* Authorization and Policies
* Scheduler
* Queuing
* Blade templating

### What is this repository for? ###

* Quick summary
Demo app Laravel
* Version
v1.0
* Demo app Laravel
https://bitbucket.org/scleijnen/demo_laravel/

### What is needed? ###

What is needed before this box can be used:

* Install VirtualBox 5.1: [https://www.vagrantup.com/downloads.html](https://www.vagrantup.com/downloads.html)
* Install Vagrant 5.1.14 [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)
* Install Git 2.11.0 [https://git-scm.com/downloads](https://git-scm.com/downloads)
* Install Composer curl -sS https://getcomposer.org/installer | php

If you want Composer to be accessible globally by simply typing composer: 
Move it to /usr/bin/ and create an alias:
sudo mv composer.phar /usr/local/bin/
and vim ~/.bash_profile

### How do I get set up? ###

1. Install Homestead Vagrant Box
    * Vagrant box add laravel/homestead
    * Chose option 2: VirtualBox
2. Installing Homestead 
    * git clone https://github.com/laravel/homestead.git Homestead (within home dir)
    * cd Homestead
    * git checkout v5.3.2 (currently the latest on 7-6-2017, can be checked here: https://github.com/laravel/homestead/releases)
    * bash init.sh
3. Setting up provider
    * Edit homestead.yaml:
    * Edit to: provider: virtualbox
    * Add type: type: �nfs� for the fastest filesharing
    * For windows the nfs plugin is needed to get sharing working: vagrant plugin install vagrant-winnfsd
    * Change - map: homestead.app to desired name: for example demo.app
    * Configure the folders and sites
4. Update host file
    * Sudo nano /private/etc/hosts
    * For example: 192.168.10.10  demo.app
5. Create or get a project
    * Empty project: composer create-project --prefer-dist laravel/laravel demo
    * Demo project: git clone https://bitbucket.org/scleijnen/demo_laravel/
6. Vagrant reload (for getting latest vagrant config)
    * vagrant reload --provision
7. DB Setup local
    * Name: Demo Laravel
    * Host: 127.0.0.1
    * Username: homestead
    * Password: secret
    * Database: (optional, whatever you want if you want to directly connect)
    * Port: 33060
8. Run composer for dependencies
    * composer install (from local or within vagrant box)
9. Run laravel migrations 
    * php artisan migrate
10. composer global require "acacha/adminlte-laravel-installer=~3.0"
11. Open app
    * http://demo.app/login (as configured earlier within vhost)


### Contribution guidelines ###

* PHP following PSR code style
* Writing tests (PHPunit)
* Code review

### Who do I talk to? ###

* Repo owner and admin Sjoerd Cleijnen

### Handy Laravel console command for showing all commands ###
* php artisan